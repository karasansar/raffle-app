<?php

namespace Database\Factories;

use App\Models\Raffle;
use Illuminate\Database\Eloquent\Factories\Factory;

class RaffleFactory extends Factory
{
    public $STATUS = ['brodcast', 'passive', 'task'];

    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Raffle::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'author' => rand(1, 10),
            'title' => $this->faker->name,
            'description' => $this->faker->text,
            'status' => $this->STATUS[rand(0,2)],
            'winner' => rand(5,15),
        ];
    }
}
