<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class RaffleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Raffle::factory(10)->create();
    }
}
