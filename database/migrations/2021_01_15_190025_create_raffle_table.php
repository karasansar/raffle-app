<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRaffleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('raffle', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('author');
            $table->text('title');
            $table->longText('description');
            $table->text('raffle_photo_path')->nullable();
            $table->enum('status', ['brodcast', 'passive', 'task'])->default('brodcast');
            $table->unsignedBigInteger('winner')->nullable();
            $table->timestamps();
            $table->foreign('author')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('winner')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('raffle');
    }
}
