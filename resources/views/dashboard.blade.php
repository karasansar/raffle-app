<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Çekilişlerim') }}
        </h2>
    </x-slot>

    <div class="container-fluid mt-5">
        <div class="row">
            <div class="col-md-12 mt-5">
                <div class="card mt-5">
                    <div class="card-body">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-6">
                                    <form method="GET" action="{{route('dashboard')}}">
                                        <div class="float-left">
                                            <input placeholder="Ara..." class="border-1 border-info"
                                                style="border-radius: 5px;" type="text" name="title"
                                                value="{{request()->get('title')}}">
                                        </div>
                                    </form>
                                </div>
                                <div class="col-md-6">
                                    <div class="float-right mr-2">
                                        <a href="{{route('dashboard.create')}}" class="btn btn-sm btn-info">Çekiliş
                                            oluştur</a>
                                    </div>
                                </div>
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="float-right">
                                                <span style="font-size: 10px; opacity: 0.7">Yayında: <i
                                                        class="fa fa-circle mr-2 text-success"></i> Taslak: <i
                                                        class="fa fa-circle mr-2 text-warning"></i> Pasif: <i
                                                        class="fa fa-circle mr-2 text-danger"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-text">
                            <table class="table">
                                <thead class="table-info">
                                    <tr>
                                        <th>Başlık</th>
                                        <th>Açıklama</th>
                                        <th>Fotoğraf</th>
                                        <th>Durum</th>
                                        <th>Kazanan</th>
                                        <th>Tarih</th>
                                        <th>İşlemler</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($raffles as $raffle)
                                    <tr>
                                        <th scope="row">{{$raffle->title}}</th>
                                        <td>{{\Illuminate\Support\Str::limit($raffle->description, $limit=50, $end = ' .....')}}</td>
                                        <td>@if($raffle->raffle_photo_path)<img src="{{$raffle->raffle_photo_path}}"
                                                alt="{{$raffle->title}}" width="35%">@else
                                            <span>-</span>@endif</td>
                                        <td>
                                            <i
                                                class="fa fa-circle mr-2 @if($raffle->status == 'passive') text-danger @elseif($raffle->status == 'brodcast') text-success @else text-warning @endif"></i>
                                        </td>
                                        <td>
                                            @if($raffle->winners)
                                            {{$raffle->winners->name}}
                                            @else
                                            Kazanan Yok
                                            @endif
                                        </td>
                                        <td>{{$raffle->created_at}}</td>
                                        <td>
                                            <!--Edit-->
                                            <form id="edit" method="GET"
                                                action="{{route('dashboard.edit', $raffle->id)}}">
                                                <button type="submit" title="Düzenle"
                                                    class="btn btn-warning text-center m-2"
                                                    style="vertical-align: middle;"><i class=" fa fa-pen-square
                                                text-white"></i></button>
                                            </form>

                                            <!--delete-->
                                            <form id="delete" method="POST"
                                                action="{{route('dashboard.destroy',$raffle->id)}}">
                                                @csrf @method('DELETE')
                                                <button type="submit" title="Sil" class="btn btn-danger text-center m-2"
                                                    style="vertical-align: middle;">
                                                    <i class="fa fa-times-circle  text-white"></i> </button>
                                            </form>

                                            <form id="show" method="GET" action="{{route('dashboard.show', $raffle->id)}}">
                                                <!--see-->
                                                <button type="submit" title="Görüntüle" class="btn btn-info text-center m-2"
                                                    style="vertical-align: middle;"><i
                                                        class="fa fa-eye text-white"></i></button>
                                            </form>

                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            @if(count($raffles) > 0)
                            <div class="col-md-12">
                                <div class="float-right">{{$raffles->links('pagination::bootstrap-4')}}</div>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>

