<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">

            {{ __('Çekilişini Oluştur') }}
        </h2>
    </x-slot>
    <div class="container-fluid mt-5">
        @include('sweetalert::alert')
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">

                        <form id="store" method="post" action="{{route('dashboard.store')}}"
                            enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label for="title" class="col-form-label">Başlık:</label>
                                <input name="title" type="text" class="form-control" id="title" required>
                            </div>

                            <div class="form-group">
                                <input type="hidden" name="author" value="{{auth()->user()->id}}" required>
                            </div>

                            <div class="form-group">
                                <label for="description">Açıklama</label>
                                <textarea name="description" class="form-control" id="description" rows="3"
                                    required></textarea>
                            </div>

                            <div class="form-group">
                                <label for="status">Çekiliş resmi</label>
                                <div class="custom-file">
                                    <input name="raffle_photo_path" type="file" class="custom-file-input"
                                        id="customFileLang" lang="tr">
                                    <label class="custom-file-label" for="photo">Resim Seç...</label>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="status">Durum</label>
                                <select class="form-control" name="status" id="status" required>
                                    <option value="brodcast">Yayında</option>
                                    <option value="passive">Pasif</option>
                                    <option value="task">Taslak</option>
                                </select>
                            </div>

                    </div>
                    <div class="modal-footer">
                        <a href="{{route('dashboard')}}" class="btn btn-secondary">Geri</a>
                        <button type="submit" class="btn btn-info">Çekiliş Oluştur</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    </div>
</x-app-layout>

