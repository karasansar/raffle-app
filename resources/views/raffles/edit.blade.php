@foreach($raffle as $raffle)
<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">

            {{ __($raffle->title) }}
        </h2>
    </x-slot>
    <div class="container-fluid mt-5">
        @include('sweetalert::alert')
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">

                        <form id="update" method="POST" action="{{route('dashboard.update', $raffle->id)}}"
                            enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <div class="form-group">
                                <label for="title" class="col-form-label">Başlık:</label>
                                <input name="title" type="text" class="form-control" id="title" required
                                    value="{{$raffle->title}}">
                            </div>

                            <div class="form-group">
                                <label for="description">Açıklama</label>
                                <textarea name="description" class="form-control" id="description" rows="3"
                                    required>{{$raffle->description}}</textarea>
                            </div>

                            <div class="form-group">
                                @if($raffle->raffle_photo_path)
                                <div class="" align="center"><img
                                        src="{{asset("/storage/".$raffle->raffle_photo_path)}}" alt="{{$raffle->title}}"
                                        width="25%"></div>@else<span class="text-info" style="opacity: 0.7">Bu çekilişin
                                    fotoğrafı bulunmamakta.</span>@endif<br>
                                <label for="status">Çekiliş resmi</label>
                                <div class="custom-file">
                                    <input name="raffle_photo_path" type="file" class="custom-file-input"
                                        id="customFileLang" lang="tr">
                                    <label class="custom-file-label" for="photo">Resim Seç...</label>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="status">Durum</label>
                                <select class="form-control" name="status" id="status" required>
                                    @if($raffle->status == 'brodcast')
                                    <option value="{{$raffle->status}}">Yayında</option>
                                    <option value="passive">Pasif</option>
                                    <option value="task">Taslak</option>
                                    @elseif($raffle->status == 'passive')
                                    <option value="{{$raffle->status}}">Pasif</option>
                                    <option value="brodcast">Yayında</option>
                                    <option value="task">Taslak</option>
                                    @else
                                    <option value="{{$raffle->status}}">Taslak</option>
                                    <option value="passive">Pasif</option>
                                    <option value="brodcast">Yayında</option>
                                    @endif
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="winner" class="col-form-label">Kazanan:</label>

                                <select class="custom-select" name="winner">

                                    @if($raffle->participants)
                                    @if($raffle->winners)
                                    <option selected value="{{$raffle->winners->id}}">{{$raffle->winners->name}}
                                    </option>
                                    @elseif(empty($raffle->winners))
                                    <option selected value="">Belli değil</option>
                                    @endif



                                    @foreach($lottery_participants as $lottery_participant)
                                    @if($raffle->winners)
                                    @if($raffle->winners->id != $lottery_participant->users->id)
                                    <option value="{{$lottery_participant->users->id}}">
                                        {{$lottery_participant->users->name}}
                                    </option>
                                    @endif
                                    @else
                                    <option value="{{$lottery_participant->users->id}}">
                                        {{$lottery_participant->users->name}}
                                    </option>
                                    @endif
                                    @endforeach

                                    @if(!empty($raffle->winners))
                                    <option value="">Belli değil</option>
                                    @endif
                                    @endif
                                </select>
                            </div>
                    </div>
                    <div class="modal-footer">
                        <a href="{{ route('dashboard') }}" class="btn btn-secondary">Geri</a>
                        <button type="submit" class="btn btn-info">Düzenle</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    </div>
</x-app-layout>
@endforeach

