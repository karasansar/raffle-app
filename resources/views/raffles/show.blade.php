<x-app-layout>
    <x-slot name="header"></x-slot>

    <div align="center" class="container mt-5">
        <div class="row">
            <div class="card rounded bg-dark shadow-lg ">
                @foreach($raffle as $r)

                @if($r->raffle_photo_path)
                <img src="{{asset('storage/'.$r->raffle_photo_path)}}" class="card-img-top" />
                @else
                <img src="https://gympielandcare.org.au/wp-content/uploads/raffle.png" class="card-img-top" />
                @endif
                <div style="width: 80%; margin: 0 auto; text-align: left" class="mt-4">
                    <h2 class="text-light">{{$r->title}}</h2>
                    @if(auth()->user()->id != $r->authors->id)
                    @if($participant->count() > 0)
                    <form id="participant_delete" method="POST" action="{{route('participants.destroy', $r->id)}}">
                        @method('DELETE')
                        @csrf
                        <button type="submit" class="btn btn-md btn-danger text-white mb-3"
                            onclick="document.getElementById('participant_delete').submit();">ÇEKİLİŞTEN AYRIL</button>
                        <div class="badge badge-info text-wrap mt-2" style="width: 6rem;"><span>Çekilişe
                                Kayıtlısınz!</span></div>
                    </form>
                    @endif
                    @if($participant->count() == 0)
                    <form id="participant_store" method="POST" action="{{route('participants.store')}}">
                        @csrf
                        <button @if($r->winners)disabled @endif type="submit" class="btn btn-md btn-info"
                            onclick="document.getElementById('participant_store').submit()">
                            ÇEKİLİŞE KATIL</button>
                        <input type="hidden" name="raffle_id" value="{{$r->id}}">
                        <input type="hidden" name="user_id" value="{{auth()->user()->id}}">
                    </form>
                    @endif
                    @endif
                    <div class="row mt-3">
                        <div class="col-md-8 text-light">

                            <p class="mt-2">{{$r->description}}</p>


                        </div>
                        <div class="col-md-1"></div>
                        <div class="col-md-3 text-light">
                            <span class="text-secondary float-left">Katılımcılar:&nbsp; </span>
                            <ul class="widget-user-list">
                                <?php $total_lottery_participants = count($lottery_participants)?>
                                @foreach($lottery_participants->slice(0, 3) as $key=>$lottery_participants)

                                <li><a href="#"
                                        class="flex text-sm border-2 border-transparent rounded-full transition duration-150 ease-in-out"
                                        data-toggle="tooltip" data-placement="top"
                                        title="{{$lottery_participants->users->name}}"><img
                                            src="{{ $lottery_participants->users->profile_photo_url }}"
                                            alt="{{$lottery_participants->users->name}}"></a>
                                </li>

                                @endforeach
                                @if($total_lottery_participants > 3)
                                <li class="number"><a>
                                        <?php $list_paticipant = $total_lottery_participants - 3;?>
                                        +{{$list_paticipant}}
                                    </a></li>
                                @elseif($total_lottery_participants > 0)
                                <span></span>
                                @else
                                <span class="text-light">Katılımcı Yok!</span>
                                @endif
                            </ul>





                            <span class="text-secondary float-left">Oluşturan: &nbsp;</span><span class="text-light"><a
                                    href="#"
                                    class="flex text-sm border-2 border-transparent rounded-full transition duration-150 ease-in-out"
                                    data-toggle="tooltip" data-placement="top" title="{{$r->authors->name}}"><img
                                        class="rounded-circle" width="35" height="35"
                                        src=" {{$r->authors->profile_photo_url}}  "
                                        alt="{{$r->authors->name}}"></a></span>

                            <span class="text-secondary float-left">Kazanan: &nbsp;</span><span>
                                @if($r->winners)
                                <a  href="#"
                                    class="flex text-sm border-2 border-transparent rounded-full transition duration-150 ease-in-out"
                                    data-toggle="tooltip" data-placement="top" title="{{$r->winners->name}}"><img
                                        class="rounded-circle" width="35" height="35"
                                        src=" {{$r->winners->profile_photo_url}}  " alt="{{$r->winners->name}}"></a>
                                @else
                                Kazanan Yok!
                                @endif
                            </span>
                        </div>
                        <div class="col-md-12 mt-5">

                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
            <div align="center" class="col-md-12">
                <footer class="container">
                    <p>&copy; Created By Doğukan Karasansar Since 2020 - {{date('Y')}}</p>
                </footer>
            </div>
        </div>
        <style>
            .widget-user-list>li {
                display: inline-block;
            }

            .widget-user-list {
                padding: 0;
                list-style-type: none;
            }

            .widget-user-list {
                margin: 0;
                white-space: nowrap;
                overflow: hidden;
            }

            .widget-user-list>li+li {
                margin-left: -1.125rem;
            }

            .widget-user-list>li a {
                display: block;
                border: 0.125rem solid #0000;
                overflow: hidden;
                width: 2.25rem;
                height: 2.25rem;
                margin-bottom: -0.3125rem;
                line-height: 2rem;
                text-align: center;
                text-decoration: none;
                -webkit-border-radius: 2.25rem;
                -moz-border-radius: 2.25rem;
                border-radius: 2.25rem;
            }

            .widget-user-list>li a img {
                display: block;
                max-width: 100%;
            }

            .widget-user-list>li.number a {
                background: #5bc0de;
                color: #fff;
            }
        </style>
</x-app-layout>




{{-- @foreach($raffle as $raffle)
<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __($raffle->title) }}
</h2>
</x-slot>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12 mt-2">
            <div class="card bg-white">

                <div class="card md-3">
                    <div class="row no-gutters">
                        <div class="col-md-6">
                            <img src="{{asset('storage/'.$raffle->raffle_photo_path)}}" class="bd-placeholder-img"
                                width="100%" height="250" />
                        </div>
                        <div class="col-md-6">
                            <div class="card-body">
                                <p class="card-text">
                                    <span class="float-right">@if(auth()->user()->id != $raffle->authors->id)
                                        @if($participant->count() > 0)
                                        <form id="participant_delete" method="POST"
                                            action="{{route('participants.destroy', $raffle->id)}}">
                                            @method('DELETE')
                                            @csrf
                                            <span title="Bu çekilişe katıldınız" class="float-right mr-5 mt-2"><a
                                                    href="javascript:;"
                                                    onclick="document.getElementById('participant_delete').submit();"><i
                                                        style="font-size: 25px"
                                                        class="fas fa-check text-success"></i></a></span></form>

                                        @endif
                                        @if($participant->count() == 0)
                                        &nbsp;<form id="participant_store" method="POST"
                                            action="{{route('participants.store')}}">
                                            @csrf<span class="float-right mr-5 mt-0" title="çekilişe katıl"><a
                                                    href="javascript:;"
                                                    onclick="document.getElementById('participant_store').submit();"><i
                                                        style="font-size: 25px"
                                                        class="fas fa-plus-circle text-info"></i></a></span><input
                                                type="hidden" name="raffle_id" value="{{$raffle->id}}"><input
                                                type="hidden" name="user_id" value="{{auth()->user()->id}}"></form>
                                        @endif
                                        @endif
                                    </span>
                                </p>
                                <br>
                                <h5 class="card-title"><span>{{$raffle->title}}</span></h5>

                                <p class="card-text">{{$raffle->description}}</p>
                                <br>
                                <p class="card-text mt-5"><small class="text-muted float-left mt-2">Başlangıç tarihi
                                        -
                                        {{$raffle->created_at->format('m/d/Y')}}</small><small
                                        class=" mb-3 text-muted float-right">
                                        @if (Laravel\Jetstream\Jetstream::managesProfilePhotos())
                                        <button
                                            class="disabled flex text-sm border-2 border-transparent rounded-full focus:outline-none  transition duration-150 ease-in-out"
                                            data-toggle="tooltip" data-placement="top"
                                            title="{{$raffle->authors->name}}">

                                            <p class="card-text mt-1"><small class="text-muted">Çekiliş Oluşturan:
                                                </small></p> <img class="h-8 w-8 rounded-full object-cover ml-2"
                                                src="{{ $raffle->authors->profile_photo_url }}"
                                                alt="{{ $raffle->authors->name }}" />
                                        </button>
                                        @else
                                        <span class="text-muted mt-2">Çekiliş Oluşturan: </span>
                                        <span class="inline-flex rounded-md">
                                            <button type="button"
                                                class="inline-flex items-center px-3 py-2 border border-transparent text-sm leading-4 font-medium rounded-md text-gray-500 bg-white hover:text-gray-700 focus:outline-none transition ease-in-out duration-150">
                                                {{ $raffle->authors->name }}

                                                <svg class="ml-2 -mr-0.5 h-4 w-4" xmlns="http://www.w3.org/2000/svg"
                                                    viewBox="0 0 20 20" fill="currentColor">
                                                    <path fill-rule="evenodd"
                                                        d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                                                        clip-rule="evenodd" />
                                                </svg>
                                            </button>
                                            @endif
                                    </small>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>


<div class="container-fluid" align="center">
    <div class="row">
        <div class="col-md-2 float-left mt-5">
            <div class="card bg-white">
                <div class="container">
                    <h4 class="text-center text-info">KATILIMCILAR</h4>
                    <div class="row">
                        <br>
                        <div class="widget">
                            <div class="widget-content">
                                <ul class="widget-user-list">
                                    <?php $total_lottery_participants = count($lottery_participants)?>
                                    @foreach($lottery_participants->slice(0, 3) as $key=>$lottery_participants)

                                    <li><a href="#"
                                            class="flex text-sm border-2 border-transparent rounded-full transition duration-150 ease-in-out"
                                            data-toggle="tooltip" data-placement="top"
                                            title="{{$lottery_participants->users->name}}"><img
                                                src="{{ $lottery_participants->users->profile_photo_url }}"
                                                alt="{{$lottery_participants->users->name}}"></a>
                                    </li>

                                    @endforeach
                                    @if($total_lottery_participants > 3)
                                    <li class="number"><a>
                                            <?php $list_paticipant = $total_lottery_participants - 3;?>
                                            +{{$list_paticipant}}
                                        </a></li>
                                    @elseif($total_lottery_participants > 0)
                                    <span></span>
                                    @else
                                    <span class="text-center text-warning mb-5">Katılımcı Yok!</span>
                                    @endif
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style>
        body {
            background: #dcdcdc;
        }

        .widget {
            background: #fff;
            margin-bottom: .75rem;
            display: block;
            position: relative;
            margin-top: 25px;
            margin-right: 20px;
        }

        .widget-user-list>li {
            display: inline-block;
        }

        .widget-user-list {
            padding: 0;
            list-style-type: none;
        }

        .widget-user-list {
            margin: 0;
            white-space: nowrap;
            overflow: hidden;
        }

        .widget-user-list>li+li {
            margin-left: -1.125rem;
        }

        .widget-user-list>li a {
            display: block;
            border: 0.125rem solid #fff;
            overflow: hidden;
            width: 2.25rem;
            height: 2.25rem;
            margin-bottom: -0.3125rem;
            line-height: 2rem;
            text-align: center;
            text-decoration: none;
            -webkit-border-radius: 2.25rem;
            -moz-border-radius: 2.25rem;
            border-radius: 2.25rem;
        }

        .widget-user-list>li a img {
            display: block;
            max-width: 100%;
        }

        .widget-user-list>li.number a {
            background: #c7c7cc;
            color: #fff;
        }

        .widget-content,
        .widget-footer {
            padding: .625rem;
            background: #fff;
            position: relative;
        }
    </style>
    </x-app-layout>
    @endforeach
    --}}

