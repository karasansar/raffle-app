<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Hoşgeldin ').auth()->user()->name }}
        </h2>
    </x-slot>

    <div class="container mt-5">
        @include('sweetalert::alert')
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <section>
                            <div class="container py-5">
                                <div class="row text-center align-items-end">
                                    <div class="col-md-12 mb-lg-0">
                                        <div class="bg-white p-5 rounded-lg shadow">
                                            <h1 class="h6 text-uppercase font-weight-bold mb-4">Kullanıcılar</h1>
                                            <h2 class="h1 font-weight-bold">{{count($users)}}<span
                                                    class="text-small font-weight-normal ml-2">/ Kişi</span></h2>

                                            <div class="custom-separator my-4 mx-auto bg-primary"></div>

                                            <ul class="list-unstyled my-5 text-small text-left">

                                                @foreach($users->take(10) as $user)
                                                <li class="mb-3">
                                                    <a href="{{route('user.show',$user->id)}}"
                                                        class="btn btn-primary text-center"
                                                        style="vertical-align: middle;"><i
                                                            class="fa fa-arrow-right mr-2 text-white"></i></a>
                                                    {{$user->name}} - {{$user->email}}
                                                </li>
                                                @endforeach
                                            </ul>
                                            <a href="{{route('user.index')}}"
                                                class="btn btn-primary btn-block p-2 shadow rounded-pill">Kullanıcılar ayarı</a>
                                        </div>
                                    </div>

                                    <div class="col-md-12 mt-5">
                                        <div class="bg-white p-5 rounded-lg shadow">
                                            <h1 class="h6 text-uppercase font-weight-bold mb-4">Çekilişler</h1>
                                            <h2 class="h1 font-weight-bold">{{count($raffles)}}<span
                                                    class="text-small font-weight-normal ml-2">/ Çekiliş</span></h2>

                                            <div class="custom-separator my-4 mx-auto bg-primary"></div>

                                            <ul class="list-unstyled my-5 text-small text-left font-weight-normal">
                                                @foreach($raffles->take(10) as $raffle)
                                                <li class="mb-3">
                                                    <i
                                                        class="@if($raffle->status == 'brodcast')fa fa-check text-primary @endif @if($raffle->status == 'task')fas fa-thumbtack text-warning @endif @if($raffle->status == 'passive')fas fa-times-circle text-danger @endif mr-2 "></i>{{$raffle->title}}
                                                    -
                                                    {{$raffle->created_at}}</li>
                                                @endforeach
                                            </ul>
                                            <a href="{{route('raffle.index')}}"
                                                class="btn btn-primary btn-block p-2 shadow rounded-pill">Çekilişler ayarı</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
<style>
    .rounded-lg {
        border-radius: 1rem !important;
    }

    .text-small {
        font-size: 0.9rem !important;
    }

    .custom-separator {
        width: 5rem;
        height: 6px;
        border-radius: 1rem;
    }

    .text-uppercase {
        letter-spacing: 0.2em;
    }

    body {
        background: #00B4DB;
        background: -webkit-linear-gradient(to right, #0083B0, #00B4DB);
        background: linear-gradient(to right, #0083B0, #00B4DB);
        color: #514B64;
        min-height: 100vh;
    }
</style>
