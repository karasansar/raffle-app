<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Çekilişler') }}
        </h2>
    </x-slot>

    <div class="container-fluid m-2 p-0 mt-5">
        <div class="row">
            <div class="col-md-3">
                <div class="container bg-white rounded shadow shadow-sm">
                    <nav class="nav p-3">
                        <form action="#">
                            <h3>Çekiliş Filtre <span class="badge badge-info">Filtre</span></h3>
                            <ul class="nav flex-column">
                                <li class="nav-item mt-4">

                                    <input class="form-control form-control-md border-info shadow shadow-sm"
                                        placeholder="Ara..." type="text" name="title">

                                </li>
                                <li class="nav-item mt-4">
                                    <select disabled class="form-control form-control-md border-info shadow shadow-sm">
                                        <option>Yakında!</option>
                                    </select>
                                </li>
                                <li class="nav-item mt-4">
                                    <input name="created_at"
                                        class="form-control form-control-md border-info shadow shadow-sm" type="date"
                                        name="created_at">
                                </li>
                                <li class="nav-item mt-4">
                                    <button type="submit" class="btn btn-md btn-block btn-info">Filtrele</button>
                                </li>
                            </ul>

                        </form>
                    </nav>
                </div>
                {{-- Future Category --}}
                {{--  <div class="container bg-white rounded shadow shadow-sm mt-5">
                    <nav class="nav p-3">
                        <h3>Çekiliş sayıları <span class="badge badge-info">{{$total_raffles}}</span></h3>
                <ul class="list-group mt-4">
                    <li class="list-group-item d-flex justify-content-between align-items-center">
                        Cras justo odio
                        <span class="badge badge-info badge-pill">14</span>
                    </li>
                    <li class="list-group-item d-flex justify-content-between align-items-center">
                        Dapibus ac facilisis in
                        <span class="badge badge-info badge-pill">2</span>
                    </li>
                    <li class="list-group-item d-flex justify-content-between align-items-center">
                        Morbi leo risus
                        <span class="badge badge-info badge-pill">1</span>
                    </li>
                </ul>
                </nav>
            </div>--}}
        </div>
        <div class="col-md-8">
            <div align="center" class="container p-5 bg-white rounded shadow shadow-sm">
                <div class="row">

                    @foreach($raffles as $raffle)
                    <div class="col-md-5 m-4">
                        <form method="GET" action="{{route('dashboard.show', $raffle->id)}}">
                            <div style="display: inline-block;
                            text-align: left;" class="card shadow shadow-sm" style="width: 18rem;">
                                <img style="height: 200px" src="@if(isset($raffle->raffle_photo_path)){{asset('storage/'.$raffle->raffle_photo_path)}}
                                 @else https://gympielandcare.org.au/wp-content/uploads/raffle.png @endif"
                                    class="card-img-top" alt="{{$raffle->title}}">
                                <div class="card-body">
                                    <h5 class="card-title text-secondary">{{$raffle->title}}</h5>
                                    <p class="card-text">
                                        {{Str::limit($raffle->description, $limit=35, $end = '...')}}.</p>
                                    <button class="btn btn-info">Çekilişe
                                        git</button>
                                    <button
                                        class="float-right disabled flex text-sm border-2 border-transparent rounded-full focus:outline-none  transition duration-150 ease-in-out"
                                        data-toggle="tooltip" data-placement="top" title="{{$raffle->authors->name}}">

                                        <p class="card-text mt-1"><small class="text-muted">
                                            </small></p> <img class="h-9 w-9 rounded-full object-cover ml-2"
                                            src="{{ $raffle->authors->profile_photo_url }}"
                                            alt="{{ $raffle->authors->name }}" />
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>

        <div class="col-md-3"></div>
        <div class="col-md-8 m-4">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="float-right">{{$raffles->links('pagination::bootstrap-4')}}</div>
                    </div>
                </div>

            </div>
        </div>
        <div align="center" class="col-md-12">
            <footer class="container">
                <p>&copy; Created By Doğukan Karasansar Since 2020 - {{date('Y')}}</p>
            </footer>
        </div>
    </div>
    </div>
    <style>
        body {
            --tw-bg-opacity: 1;
            background-color: rgba(243, 244, 246, var(--tw-bg-opacity));
        }
    </style>
    {{--  <script>
        $('#getData').click(function() {
                console.log({{$raffle->id}});
    $.ajax({
    type: 'GET',
    url: '/cekilislerim/show/'+{{$raffle->id}},
    success: function(data) {
    console.log(data);
    }
    })
    });
    </script> --}}
</x-app-layout>

