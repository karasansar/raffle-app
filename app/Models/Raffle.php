<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Raffle extends Model
{
    use HasFactory;

    protected $table = 'raffle';

    protected $fillable=[
    'author',
    'title',
    'description',
    'status',
    'winner',
    'raffle_photo_path'
   ];

   public function authors() {
       return $this->belongsTo('\App\Models\User', 'author');
   }

   public function winners() {
       return $this->belongsTo('\App\Models\User', 'winner');
   }

   public function users() {
    return $this->belongsToMany('\App\Models\User', '\App\Models\Participants', 'user_id');
   }

   public function participants() {
       return $this->belongsToMany('\App\Models\Raffle', '\App\Models\Participants', 'raffle_id', 'user_id');
   }

}
