<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Participants extends Model
{
    use HasFactory;

    protected $table = 'participants_transactions';

    protected $fillable = ['raffle_id', 'user_id'];

    public function raffles() {
        return $this->belongsTo('\App\Models\Raffle', 'raffle_id');
    }

    public function users() {
        return $this->belongsTo('\App\Models\User', 'user_id');
    }
}
