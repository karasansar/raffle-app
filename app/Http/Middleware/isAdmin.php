<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class isAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if(empty(auth()->user())) {
            Alert::toast('Önce Giriş Yapın!', 'warning');
            return redirect()->route('login');
        }
        if(auth()->user()->roles !== 'admin') {
            Alert::toast('YETKİNİZ YOK!', 'error');
            return redirect()->route('dashboard');
        }
        return $next($request);
    }
}
