<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Raffle;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use RealRashid\SweetAlert\Facades\Alert;

class AdminController extends Controller
{
    public function index() {
        Alert::toast('Hoşgeldin '.auth()->user()->name, 'success');
        $users = User::select('*')->orderBy('created_at', 'DESC')->get();
        $raffles = Raffle::select('*')->orderBy('created_at', 'ASC')->get();
        return view('admin.show', compact('users', 'raffles'));
    }
}
