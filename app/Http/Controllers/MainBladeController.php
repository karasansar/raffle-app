<?php

namespace App\Http\Controllers;

use App\Models\Raffle;
use Illuminate\Http\Request;

class MainBladeController extends Controller
{
    public function index()
    {
        $raffles = Raffle::where('status', 'brodcast')->orderBy('created_at', 'DESC');
        if(request()->get('title')){
            $raffles = $raffles->where('title', 'LIKE', "%".request()->get('title')."%");
        }
        if(request()->get('created_at')){
            $raffles = $raffles->where('created_at', 'LIKE', "%".request()->get('created_at')."%");
        }
        if(request()->get('created_at') && request()->get('title')){
            $raffles = $raffles->where('created_at', 'LIKE', "%".request()->get('created_at')."%")->where('title', 'LIKE', "%".request()->get('title')."%");
        }
        $raffles = $raffles->paginate(6);
       /*  $total_raffles = count(Raffle::all()); */
        return view('welcome', compact('raffles'));
    }
}
