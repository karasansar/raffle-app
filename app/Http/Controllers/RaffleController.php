<?php

namespace App\Http\Controllers;

use App\Models\Participants;
use App\Models\Raffle;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Str;
use Symfony\Component\Console\Input\Input;

class RaffleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $raffles = Raffle::where('author', auth()->user()->id)->with(['authors', 'winners', 'participants.users'])->orderBy('created_at', 'DESC');
        if(request()->get('title')) {
            $raffles = $raffles->where('title', 'LIKE', "%".request()->get('title')."%");
        }

        $raffles = $raffles->paginate(5);

        return view('dashboard', compact('raffles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('raffles.store');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $raffle = $request->validate([
            'title' => 'required',
            'description' => 'required',
            'status' => 'required',
            'raffle_photo_path' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'author' => 'required'
        ]);

        if($request->raffle_photo_path) {
             Image::make($request->file('raffle_photo_path')->getRealPath())->resize(1350,600)->save();
             $raffle['raffle_photo_path'] = $request->raffle_photo_path->store('raffles');
        }
        Raffle::create($raffle);

        Alert::toast('Çekiliş Başarıyla Oluşturuludu!', 'success');
        return redirect()->route('dashboard');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $raffle = Raffle::where('id', $id)->with(['authors', 'winners', 'participants.users'])->get();
        $participant = Participants::where('user_id', auth()->user()->id)->where('raffle_id', $id)->with('users')->get();
        $lottery_participants = Participants::where('raffle_id', $id)->with('users')->orderBy('created_at', 'DESC')->get();

        return view('raffles.show',compact('raffle', 'participant', 'lottery_participants'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $raffle = Raffle::where('id', $id)->with(['authors', 'winners', 'participants.users'])->get();
        $lottery_participants = Participants::where('raffle_id', $id)->with('users')->get();
        foreach ($raffle as $authors) {
            if(auth()->user()->id == $authors->authors->id) {
                return view('raffles.edit', compact('raffle', 'lottery_participants'));
            }
            else {
                Alert::toast('Giriş Yetkiniz Yok!', 'error');
                return redirect()->route('dashboard');
            }
        }

        Alert::toast('Üzgünüz Çekiliş Bulanamadı', 'warning');
        return redirect()->route('dashboard');


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $raffle = $request->validate([
            'title' => 'required',
            'description' => 'required',
            'status' => 'required',
            'raffle_photo_path' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);


        $raffle['winner'] = $request->winner;
        if($request->raffle_photo_path) {
            Image::make($request->file('raffle_photo_path')->getRealPath())->resize(1350,600)->save();
            $raffle['raffle_photo_path'] = $request->raffle_photo_path->store('raffles');
        }

        Raffle::where('id', $id)->update($raffle);


        Alert::toast('Çekiliş Başarıyla Güncellendi', 'success');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $raffle = Raffle::where('id', $id)->with(['authors'])->get();
        foreach ($raffle as $authors) {
            if(auth()->user()->id == $authors->authors->id) {
                $raffle = Raffle::where('id', $id)->delete();
                Alert::toast('Çekiliş Başarıyla Kaldırıldı', 'success');
        return redirect()->route('dashboard');
            } else {
                Alert::toast('Giriş Yetkiniz Yok!', 'error');
                return redirect()->back();
            }
        }
    }
}
