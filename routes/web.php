<?php
use Illuminate\Support\Facades\Route;
//Admin Controller
use App\Http\Controllers\admin\AdminController;
use App\Http\Controllers\admin\RaffleController;
use App\Http\Controllers\admin\UserController;
use App\Http\Controllers\MainBladeController;
use App\Http\Controllers\ParticipantController;
//Main Controller
use App\Http\Controllers\RaffleController as MainControllerRaffle;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',[MainBladeController::class, 'index'])->name('welcome');

//Raffle Route
Route::middleware(['auth:sanctum', 'verified'])->get('/cekilislerim', [MainControllerRaffle::class, 'index'])->name('dashboard');
Route::middleware(['auth:sanctum', 'verified'])->post('/cekilislerim', [MainControllerRaffle::class, 'store'])->name('dashboard.store');
Route::middleware(['auth:sanctum', 'verified'])->get('/cekilislerim/show/{id}', [MainControllerRaffle::class, 'show'])->name('dashboard.show');
Route::middleware(['auth:sanctum', 'verified'])->get('/cekilislerim/create', [MainControllerRaffle::class, 'create'])->name('dashboard.create');
Route::middleware(['auth:sanctum', 'verified'])->delete('/cekilislerim/{id}', [MainControllerRaffle::class, 'destroy'])->name('dashboard.destroy');
Route::middleware(['auth:sanctum', 'verified'])->get('/cekilislerim/{id}/edit', [MainControllerRaffle::class, 'edit'])->name('dashboard.edit');
Route::middleware(['auth:sanctum', 'verified'])->put('/cekilislerim/{id}', [MainControllerRaffle::class, 'update'])->name('dashboard.update');

//Participant Route
Route::group(['middleware' => 'auth:sanctum', 'verified'], function () {
    Route::resource('participants', ParticipantController::class);
});


// Admin Route
Route::group(['middleware' => ['isAdmin'], 'prefix' => 'admin'], function () {
    Route::get('/', [AdminController::class, 'index'])->name('admin.index');
    Route::resource('raffle', RaffleController::class);
    Route::resource('user', UserController::class);
});
